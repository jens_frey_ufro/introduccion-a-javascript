var min = document.getElementById("min");
var max = document.getElementById("max");

var boton = document.getElementById("boton");

var resultado = document.getElementById("resultado");

boton.addEventListener("click", function(){
    var numMin = min.value;
    var numMax = max.value;

    var numero = obtenerNumeroAleatorio(numMin, numMax);

    console.log(numero);

    if(!(numMin === "") && !(numMax === "")){
        if(parseInt(numMin) > parseInt(numMax)){
            resultado.innerHTML = "El rango mínimo es mayor que el rango máximo";
        } else {
            resultado.innerHTML = numero;
        }
    } else {
        resultado.innerHTML = "Ingrese ambos valores";
    }

    

});

function obtenerNumeroAleatorio(numMin, numMax){
    return Math.round((Math.random() * (numMax - numMin) ) + parseInt(numMin));
}