// Obtenemos el button y lo almacenamos en una variable llamada "btn"
var btn = document.getElementById("btn");
/* Obtenemos el div que muestra el resultado y lo
almacenamos en una variable llamada "resultado" */
var resultado = document.getElementById("resultado")
/* Obtenemos los dos input y los almacenamos en
variables "inputUno" y "inputDos" */
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");

var operador = document.getElementById("operador");

// Añadimos el evento click a la variable "btn"
btn.addEventListener("click",function(){
    /* Obtenemos el valor de cada input accediendo a
    su atributo "value" */
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    var op = operador.value;

    resultado.style.color = "black";

    if(!(n1 === "") && !(n2 === "")){
        if(esEntero(n1) && esEntero(n2)){
            switch(op){
                case '+':
                    /* Llamamos a una función que permite realizar la
                    suma de los números y los mostramos al usuario" */
                    resultado.innerHTML = suma(n1,n2);
                    break;
                case '-':
                    resultado.innerHTML = resta(n1,n2);
                    break;
                case '*':
                    resultado.innerHTML = multiplicar(n1,n2);
                    break;
                case '/':
                    if(!(n2 == 0)){
                        resultado.innerHTML = divir(n1,n2);
                    } else {
                        resultado.style.color = "red";
                        resultado.innerHTML = "No se puede dividir por 0";
                    }
                    break;
            }
        } else {
            resultado.style.color = "red";
            resultado.innerHTML = "Los valores deben ser enteros";
        }
    }else{
        resultado.style.color = "red";
        resultado.innerHTML = "Asegurese de ingresar ambos valores";
    }
    
});

/* Función que retorna la suma de dos números */
function suma(n1, n2){
    // Es necesario aplicar parseInt a cada número
    return parseInt(n1) + parseInt(n2);
}

function resta(n1, n2){
    return parseInt(n1) - parseInt(n2);
}

function multiplicar(n1, n2){
    return parseInt(n1) * parseInt(n2);
}

function divir(n1, n2){
    return parseInt(n1) / parseInt(n2);
}

function esEntero(n){
    if(n%1 === 0){
        return true;
    } else {
        return false;
    }
}
